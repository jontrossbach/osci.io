---
---

{% capture intro %}
# OSCI?

## Open Source Community Infrastructure

You might have found your way here by clicking a link on a page that uses 
osci.io services in one way or another.

Osci.io provides [infrastructure and services](/mission_and_services/) for open source communities 
and projects connected with the
[Open Source Program Office](https://community.redhat.com/) (OSPO) within
[Red Hat](https://redhat.com) (formerly known as OSAS, Open Source and Standards).
To work with us, join one of the many communities we participate in directly.
We participate in, and contribute to [opensourceinfra.org](https://opensourceinfra.org/)
to share our work ranging from content to best practices.

This website is maintained at 
[gitlab.com/osas/osci.io](https://gitlab.com/osas/osci.io) and our open 
infrastructure configuration management, scripts, content, documentation, and 
so forth are maintained in the [gitlab.com/osas](https://gitlab.com/osas/) repository

{% endcapture %}

{% capture left-column %}

## Stalk us

  * Follow [#openinfra](https://twitter.com/search?q=%23openinfra) on Twitter
    for news and updates.
  * Follow [@redhatopen](https://twitter.com/redhatopen) for updates from the
    Red Hat OSPO team.

## Projects we work on

Our team participates directly on infrastructure operations in multiple
upstream free/open source software projects:

  * [Gluster](https://www.gluster.org) 
  * [RDO Project](https://www.rdoproject.org)
  * [oVirt](https://www.ovirt.org)
  * [Project Atomic](https://www.projectatomic.io)
  * [Software Collections](https://www.softwarecollections.org)
  * [The Open Source Way](https://www.theopensourceway.org)
  * [Beaker](https://beaker-project.org/)
  * [Open Source Infrastructure](https://opensourceinfra.org)

{% endcapture %}

{% capture right-column %}

## Contact us

We want our communication and operations to be open to encourage participation 
and collaboration.

Since our team is distributed around the world, the daily operations and 
discussions happen mostly online.

### Live Discussion

The daily operations and live discussion happens on the 
[#openinfra](https://webchat.freenode.net/?channels=openinfra) channel on the 
Freenode IRC network.

### Mailing list

The [openinfra mailing list](https://lists.opensourceinfra.org/archives/) the place for open and transparent discussion for the Open Source Infrastructure community.

### Security Contact

For any questions or concerns email [security@osci.io](mailto:security@osci.io).

{% endcapture %}


<!-- ---------------------------- -->

<div class="grid frontpage">
  <div class="col-12 intro">{{ intro | markdownify }}</div>
  <div class="col-6_md-6_sm-12 left-column">{{ left-column | markdownify }}</div>
  <div class="col-6_md-6_sm-12 right-column">{{ right-column | markdownify }}</div>
</div>
