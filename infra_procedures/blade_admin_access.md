---
title: Blade Administrative Access
---

## Introduction

A web UI and Java Console are available to set up blades' power, various parameters, and control it via remote console.

To access the administrative VLAN where the IPMI interfaces are available we'll setup a SSH SOCKS proxy first.

## Establishing a SOCKS Proxy

```shell
ssh -D 9050 -f -C -q -N root@catton.osci.io
```
(or root@speedy.osci.io as fallback)

This proxy will not last forever, might get down because of network issues, or simply block, so if it stops working restarting it is often an easy fix.

From this point on we'll be using the `proxychains` tool to redirect web and console access through the tunnel (`proxychains4` package on Debian, `proxychains-ng` on Red Hat system). The configuration must include a line for port `9050` like this:
```
socks4 	127.0.0.1 9050
```
(You may change the port number in case of conflict, both here and when starting the tunnel)

## Accessing the IPMI web UIs

```shell
proxychains firefox -P communitycage-osci -no-remote https://cmm-catatonic.adm.osci.io/
```

The first time you will need to create a `communitycage-osci` profile. It's useful to have separate proxy settings independent from your usual settings.

You may now add exceptions for the Supermicro certificates as they will only be added to this profile.

From the blade status page you can open each blade's IPMI UI and also reach the console to control it.

## Notes on the Java Console

Before connecting, be sure to install the Java JRE (`openjdk-8-jre` package on Debian, `java-1.8.0-openjdk` on Red Hat systems). Unfortunately the Java Console does only work with version 8 of the JRE, so be sure to select the right one as default if multiple version are installed on your system (`update-alternatives --config java` on Debian, `alternatives --config java` on Red Hat systems).

After authenticating on the blade's IPMI UI, you get this question:
```
You need the latest Java(TM) Runtime Environment. Would you like to update now?
```
to which you can simply reply `Cancel`.

From here you control the Blade's power.

Click on the console screenshot (sometimes broken) and access the Java application. After trusting the application you can now use the remote console.

When you reboot the screen size changes many times, which is very annoying, so I would suggest using a fixed-size window by disabling the `Auto-resize window` setting in the `Options->Preference->Window` menu.

Note you can also control power from the console interface.

## Fast path to the IPMI Console

If the blade is already [declared in the Ansible inventory](/infra_procedures/blade_hw_config/), the `ipmi_console` script, provided in the `community-cage-infra-ansible` repository, allows you to quickly reach the console without having to use the web UIs:
```shell
./ipmi_console <host>
```
(with `<host>` the Ansible host name declared in the inventory)

